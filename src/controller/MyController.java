package controller;

import java.net.URL;
import java.util.ResourceBundle;

import org.openqa.selenium.chrome.ChromeDriver;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import utils.JsoupUtils;
import utils.SeleniumUtils;

public class MyController implements Initializable {

    @FXML
    private Button startBtn;
    @FXML
    private Button stopBtn;

    private static ChromeDriver driver;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void startSelenium(ActionEvent event) {
        try {
            driver = SeleniumUtils.initDriver();
            JsoupUtils.getLink("Agent Sbo");
        } catch (Exception e) {
            new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
        }
        startBtn.setText("Pause");
        startBtn.setDisable(true);
    }

    public void stopSelenium(ActionEvent event) {
        driver.quit();
    }

}
