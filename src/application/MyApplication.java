package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MyApplication extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("MyScence.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("Login.css").toExternalForm());
            primaryStage.setTitle("Sbobet");
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (IOException e) {
        }
    }

    public static void main(String... strings) {
        launch(strings);
    }

}
