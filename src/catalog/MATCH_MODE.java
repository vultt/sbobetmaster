package catalog;

import java.util.HashMap;
import java.util.Map;

public enum MATCH_MODE {

    ENEMY("enemy"), // đánh ngược
    ALLY("ally"),  // đánh thuận
    ;

    private MATCH_MODE(String v) {
        this.v = v;
    }

    private static Map<String, MATCH_MODE> vMap = new HashMap<String, MATCH_MODE>();
    static {
        for (MATCH_MODE r : MATCH_MODE.values()) {
            vMap.put(r.v, r);
        }
    }

    public static MATCH_MODE fromV(String v){
        return vMap.get(v);
    }

    public String v;

    public String getV() {
        return this.v;
    }

}
